# To the extent possible under law, the author(s) have dedicated all 
# copyright and related and neighboring rights to this software to the 
# public domain worldwide. This software is distributed without any warranty. 
# You should have received a copy of the CC0 Public Domain Dedication along 
# with this software. 
# If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 

# base-files version 4.2-4

# ~/.bash_profile: executed by bash(1) for login shells.

# The latest version as installed by the Cygwin Setup program can
# always be found at /etc/defaults/etc/skel/.bash_profile

# Modifying /etc/skel/.bash_profile directly will prevent
# setup from updating it.

# The copy in your home directory (~/.bash_profile) is yours, please
# feel free to customise it to create a shell
# environment to your liking.  If you feel a change
# would be benifitial to all, please feel free to send
# a patch to the cygwin mailing list.

# User dependent .bash_profile file

# source the users bashrc if it exists
if [ -f "${HOME}/.bashrc" ] ; then
  source "${HOME}/.bashrc"
fi

# Set PATH so it includes user's private bin if it exists
# if [ -d "${HOME}/bin" ] ; then
#   PATH="${HOME}/bin:${PATH}"
# fi

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi
###############################################################################

#  Anaconda Environment Selection - Plese set CONDA_BASE_DIR to the directory
#  containing the base installation of anaconda/miniconda.

export CONDA_BASE_DIR=/cygdrive/c/$USER/anaconda3

#  Proxy Servers & Network Setup (if needed)

export HTTP_PROXY=
export HTTPS_PROXY=

#  IMPORTANT - Ignore carriage returns when using a Cygwin environment.

export SHELLOPTS
set -o igncr

###############################################################################

#  Manage conda environments for Python.  We check the environment variable
#  $CONDA_DEFAULT_ENV to see which environment is desired.  The default (root)
#  environment will be chosen if nothing is specified.  Note that this variable
#  will be explicitly managed by the cyg-activate ( ) function we have defined
#  below, specifically for the purpose of changing environments.  The root
#  environment is also handled slightly different from the others when it comes
#  to setting the CONDA_DEFAULT_ENV variable.

if [ ${CONDA_DEFAULT_ENV} ] && [ ${CONDA_DEFAULT_ENV} != 'root' ] 
then
    #  SELECT ONE OF THE NON-DEFAULT ENVIRONMENTS
    export CONDA_PREFIX=${CONDA_BASE_DIR}/envs/${CONDA_DEFAULT_ENV}
else
    #  SELECT THE DEFAULT ENVIRONMENT (and set CONDA_DEFAULT_ENV full path)
    export CONDA_DEFAULT_ENV=root
    export CONDA_PREFIX=${CONDA_BASE_DIR}
fi

###############################################################################

#  Define cyg-conda and cyg-activate to facilitate management of conda.

alias cyg-conda=${CONDA_BASE_DIR}/Scripts/conda.exe

cyg-activate() {
    export CONDA_DEFAULT_ENV=$1
    source ~/.bash_profile
    cyg-conda info --envs
}

###############################################################################

#  PATH - ALl of the anaconda/miniconda path entries appear first.

PATH=
PATH=$PATH:$CONDA_PREFIX
PATH=$PATH:$CONDA_PREFIX/Library/mingw-w64/bin
PATH=$PATH:$CONDA_PREFIX/Library/usr/bin
PATH=$PATH:$CONDA_PREFIX/Library/bin
PATH=$PATH:$CONDA_PREFIX/Scripts
PATH=$PATH:$HOME/scripts
PATH=$PATH:$HOME/local/bin
PATH=$PATH:/usr/local/bin
PATH=$PATH:/usr/bin

export PATH

###############################################################################
export PATH=/cygdrive/c/Program\ Files/R/R-3.5.1/bin:$PATH
